package com.imamfarisi.mymurrotal;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

public class PlayerService extends Service {

    //player
    private MediaPlayer mediaPlayer = new MediaPlayer();

    //receiver
    private BroadcastReceiver broadcastReceiver;

    @Override
    public void onCreate() {
        super.onCreate();

        //init receiver
        IntentFilter filter = new IntentFilter();
        filter.addAction("playpause");
        filter.addAction("exit");
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (action.equals("playpause")) {
                    if (mediaPlayer != null) {
                        if (mediaPlayer.isPlaying()) {
                            mediaPlayer.pause();
                        } else {
                            mediaPlayer.start();
                        }
                    }
                } else {
                    stopSelf();
                }
            }
        };

        registerReceiver(broadcastReceiver, filter);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.alfatihah);
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
        showNotif();
        return START_STICKY;
    }

    private void showNotif() {
        String NOTIFICATION_CHANNEL_ID = "channel_murrotal";
        Context context = this.getApplicationContext();
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            String channelName = "Murrotal Channel";
            int importance = NotificationManager.IMPORTANCE_HIGH;

            NotificationChannel mChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, importance);
            notificationManager.createNotificationChannel(mChannel);
        }

        Intent stopIntent = new Intent(this, PlayerReceiver.class);
        stopIntent.setAction("exit");
        PendingIntent exitPendingIntent = PendingIntent.getBroadcast(this, 12345, stopIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent playPauseIntent = new Intent(this, PlayerReceiver.class);
        playPauseIntent.setAction("playpause");
        PendingIntent playPendingIntent = PendingIntent.getBroadcast(this, 12345, playPauseIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
        builder.setSmallIcon(android.R.drawable.ic_media_play)
                .setTicker("Listening Murrotal")
                .setOngoing(true)
                .setContentTitle("Surat Al-Fatihah")
                .setContentText("by imamfarisi.com")
                .addAction(android.R.drawable.ic_media_play, "PLAY/PAUSE", playPendingIntent)
                .addAction(android.R.drawable.ic_menu_close_clear_cancel, "EXIT", exitPendingIntent)
        ;

        startForeground(115, builder.build());
    }

    private void hideNotif() {
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.cancel(115);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
        }
        unregisterReceiver(broadcastReceiver);
        hideNotif();
    }
}