package com.imamfarisi.mymurrotal;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);

        Button btnPlay = findViewById(R.id.btnPlay);
        btnPlay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //playing murrotal on background
                startService(new Intent(MainActivity.this, PlayerService.class));
                Toast.makeText(MainActivity.this, "Murrotal diputar silahkan lihat notifikasi", Toast.LENGTH_LONG).show();
            }
        });
    }
}
